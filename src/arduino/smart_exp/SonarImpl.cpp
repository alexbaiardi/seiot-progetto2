#include "SonarImpl.h"
#include "Arduino.h"

SonarImpl::SonarImpl(int echoPin,int trigPin){
  this->echoPin = echoPin;
  this->trigPin = trigPin;
  pinMode(echoPin, INPUT);
  pinMode(trigPin, OUTPUT);
}
void SonarImpl::initialize(float temperature){
  /*Calculate sound speed*/
  this->vs=331.45 + 0.62*temperature;
}
float SonarImpl::getDistance(){
    /* invio impulso */
  digitalWrite(trigPin,LOW);
  delayMicroseconds(3);
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin,LOW);

  /* ricevi l’eco */
  float tUS = pulseIn(echoPin, HIGH);
  float t = tUS / 1000.0 / 1000.0 / 2;
  float d = t*vs;
  return d;
}
