#ifndef __SONARIMPL__
#define __SONARIMPL__

#include "Sonar.h"

class SonarImpl:public Sonar {

public:
  SonarImpl(int echoPin,int trigPin);
  void initialize(float temperature);
  float getDistance();


private:
  int echoPin;
  int trigPin;
  float vs;
};

#endif
