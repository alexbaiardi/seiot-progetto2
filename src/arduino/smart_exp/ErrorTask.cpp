#include "TaskImpl.h"
#include "ErrorTask.h"
#include "MsgService.h"
#include "MsgFactory.h"

ErrorTask::ErrorTask(){}

void ErrorTask::start(){
    Msg* msg=MessageFactory.createStateMessage("Error");
    MsgService.sendMsg(msg);
    delete msg;
    blinkTask->setActive(true);
    startTime=millis();
}
void ErrorTask::finish(){}
  
void ErrorTask::init(int period){
  TaskImpl::init(period);
}

void ErrorTask::tick(){
  blinkTask->setActive(false);
  this->setActive(false);
  setupTask->setActive(true);
}
void ErrorTask::setControlledTask(Task* blinkTask, Task* setupTask){
  this->blinkTask=blinkTask;
  this->setupTask=setupTask;
}
