#ifndef __EXPERIMENTTASK__
#define __EXPERIMENTTASK__

#include "TaskImpl.h"
#include "Led.h"
#include "Sonar.h"
#include "Potentiometer.h"
#include "ServoMotor.h"
#include "TemperatureSensor.h"

#define ERROR_TIME 2000
#define VALUES 1024
#define LEVELS 25
#define MIN_PERIOD 40
#define MAX_DISTANCE 1.0
#define MAX_VEL (double)4
#define MAX_ANGLE 180

/*
 Task that manage experiment. It gets, processes and communicates data.  
 */
class ExperimentTask: public TaskImpl {
private:
  Task *setupTask, *endExpTask, *errorTask;
  Led* l2;
  Sonar* sonar;
  Potentiometer* pot;
  ServoMotor* servo;
  TemperatureSensor* tempSensor;
  unsigned long startTime;
  unsigned long instant;
  double distance;
  double velocity=0.0;
  double acceleration=0.0;
  
protected:
  void start();
  void finish();
  
public:

  ExperimentTask(int ledPin, int echoPin,int trigPin, int potPin, int servoPin, int tempPin);  
  void init(int period);  
  void tick();
  void setControlledTask(Task* setupTask, Task* endExpTask, Task* errorTask);
};

#endif
