#ifndef __MSGSERVICE__
#define __MSGSERVICE__

#include "Arduino.h"
/*
 Class that represent a Message 
 */
class Msg {
  String content;

public:
  Msg(String content){
    this->content = content;
  }
  
  String getContent(){
    return content;
  }
};

/*
  Class that menage communication with Viewer
*/
class MsgServiceClass {
  
public: 
  
  Msg* currentMsg;
  bool msgAvailable;

  void init();  

  bool isMsgAvailable();
  Msg* receiveMsg();
  
  void sendMsg(const Msg* msg);
};

extern MsgServiceClass MsgService;

#endif
