#include "ExperimentTask.h"
#include "SonarImpl.h"
#include "PotentiometerImpl.h"
#include "ServoMotorImpl.h"
#include "TemperatureSensorImpl.h"
#include "MsgService.h"
#include "MsgFactory.h"
#include "Arduino.h"

ExperimentTask::ExperimentTask(int ledPin, int echoPin,int trigPin, int potPin, int servoPin, int tempPin){
  this->l2= new Led(ledPin);    
  this->sonar= new SonarImpl(echoPin,trigPin);    
  this->pot= new PotentiometerImpl(potPin);
  this->servo= new ServoMotorImpl(servoPin);
  this->tempSensor= new TemperatureSensorImpl(tempPin);
}

void ExperimentTask::setControlledTask(Task* setupTask, Task* endExpTask, Task* errorTask){
  this->setupTask=setupTask;
  this->endExpTask=endExpTask;
  this->errorTask=errorTask;
}

void ExperimentTask::init(int period){
  TaskImpl::init(period);
}

void ExperimentTask::start(){
  /*Notify state*/
  Msg* msg=MessageFactory.createStateMessage("Experiment");
  MsgService.sendMsg(msg);
  delete msg;
  /*Initialize environment*/
  l2->switchOn();
  sonar->initialize(tempSensor->getTemperature());
  distance=sonar->getDistance();
  /*Check object presence*/
  if(distance>MAX_DISTANCE){
    errorTask->setActive(true);
    this->setActive(false);
  } else{
    /*Calculate experiment period*/
    int val=pot->getValue();
    int level = val / ((float)VALUES / LEVELS) + 1;
    int period = level*MIN_PERIOD;
    this->setPeriod(period);
    
    servo->on();
    endExpTask->setActive(true);

    /*Initialize data*/
    startTime=millis();
    distance=sonar->getDistance();
    instant=millis();
    velocity=0;
    acceleration=0;
    Msg* msg=MessageFactory.createDataMessage((double)(instant-startTime)/1000, distance, velocity, acceleration);
    MsgService.sendMsg(msg);
    delete msg;
  }    
}

void ExperimentTask::finish(){
  l2->switchOff();
  servo->off();
}
  
void ExperimentTask::tick(){
  int count=0;
  double actualDistance, actualVelocity;
  unsigned long actualInstant;
  /*Get distance with control for invalid value*/
  do{
    actualDistance=sonar->getDistance();
    actualInstant=millis();
    count++;
  }while(actualDistance>MAX_DISTANCE && count<3);
  /*Check object presence*/
  if(actualDistance>MAX_DISTANCE){
    actualDistance=0;
    actualVelocity=0;
    acceleration=0;
  } else{
    /*Calculate speed and acceeration*/
    actualVelocity=(actualDistance-distance)/(actualInstant-instant)*1000;
    acceleration=(actualVelocity-velocity)/(actualInstant-instant)*1000;
  }
  distance=actualDistance;
  velocity=actualVelocity;
  instant=actualInstant;

  /*Set Servomotor position*/
  int angle=abs(velocity)/(MAX_VEL/MAX_ANGLE);
  servo->setPosition(angle);

  /*Communicate data to Viewer*/
  Msg* msg=MessageFactory.createDataMessage((double)(instant-startTime)/1000, distance, velocity, acceleration);
  MsgService.sendMsg(msg);
  delete msg;
}
