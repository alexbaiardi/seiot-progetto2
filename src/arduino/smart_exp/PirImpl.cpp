#include "PirImpl.h"
#include "Arduino.h"

PirImpl::PirImpl(int pin){
  this->pin = pin;
  pinMode(pin, INPUT);
}

int PirImpl::getPin(){
  return this->pin;
}

bool PirImpl::movement(){
  return digitalRead(pin) == HIGH;
};
