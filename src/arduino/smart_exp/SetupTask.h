#ifndef __SETUPTASK__
#define __SETUPTASK__

#include "TaskImpl.h"
#include "Led.h"
#include "Pir.h"
#include "Button.h"

#define SLEEP_TIME 5000

/*
  Task that wait for start of experiment and put in sleep mode system after SLEEP_TIME
*/
class SetupTask: public TaskImpl {

private:
  Led* l1;
  Pir* pir;
  Button* btn;
  Task* experiment;
  unsigned long startTime;

  void start();
  void finish();
  void sleep();
  
public:
  SetupTask(int pirPin, int l1Pin, int btnPin);
  void init(int period);
  void tick();
  void setControlledTask(Task* experiment);
};

#endif
