#include "MsgFactory.h"
#include "MsgService.h"

Msg* MsgFactory::createStateMessage(String state){
  return new Msg(String("state;")+state);
}

Msg* MsgFactory::createDataMessage(double instant, double distance, double velocity, double acceleration){
  return new Msg(String("data;")+instant+String(";")+distance+String(";")+velocity+String(";")+acceleration);
}

Msg* MsgFactory::createNotificationMessage(String message){
  return new Msg(String("notification;")+message);
}
