#ifndef __TASKIMPL__
#define __TASKIMPL__
#include "Task.h"

/*
 Implementation of Task that performs action on activation an deactivation
*/
class TaskImpl:public Task {
  int myPeriod;
  int timeElapsed;
  bool active;

protected:
  void setPeriod(int period){
    myPeriod = period;
  }
  virtual void start()=0;
  virtual void finish()=0;
  
public:
  void init(int period){
    this->setPeriod(period);
    timeElapsed = 0;
  }

  virtual void tick() = 0;

  bool updateAndCheckTime(int basePeriod){
    timeElapsed += basePeriod;
    if (timeElapsed >= myPeriod){
      timeElapsed = 0;
      return true;
    } else {
      return false; 
    }
  }

  bool isActive(){
    return active;
  }

  void setActive(bool active){
    this->active = active;
    if(active){
      start();
    }else{
      finish();
    }
  }
};

#endif
