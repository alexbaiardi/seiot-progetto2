#ifndef __MSGFACTORY__
#define __MSGFACTORY__
#include "MsgService.h"
/*
  Factory for message that creates message to send at Viewer.
*/
class MsgFactory{

  public:
    Msg* createStateMessage(String state);

    Msg* createDataMessage(double instant, double distance, double velocity, double acceleration);

    Msg* createNotificationMessage(String message);
};



extern MsgFactory MessageFactory;

#endif
