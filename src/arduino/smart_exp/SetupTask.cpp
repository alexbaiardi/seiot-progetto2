#include <EnableInterrupt.h>
#include "SetupTask.h"
#include <avr/sleep.h>
#include "PirImpl.h"
#include "MsgService.h"
#include "MsgFactory.h"
#include "ButtonImpl.h"
#include "Arduino.h"

SetupTask::SetupTask(int pirPin, int l1Pin, int btnPin){
  this->pir = new PirImpl(pirPin);
  this->l1 = new Led(l1Pin);
  this->btn=new ButtonImpl(btnPin);
}

void SetupTask::setControlledTask(Task* experiment){
  this->experiment=experiment;
}

void SetupTask::init(int period){
  TaskImpl::init(period);
}

void wakeUp(){}

void SetupTask::start(){
  l1->switchOn();
  startTime=millis();
  Msg* msg=MessageFactory.createStateMessage("Ready");
  MsgService.sendMsg(msg);
  delete msg;
}

void SetupTask::finish(){
  l1->switchOff();
}

void SetupTask::sleep(){
      Msg* msg=MessageFactory.createStateMessage("Sleep");
      MsgService.sendMsg(msg);
      delete msg;
      delay(15);/* time to send message*/
      finish();      
      enableInterrupt(pir->getPin(), wakeUp , CHANGE);
      set_sleep_mode(SLEEP_MODE_PWR_DOWN);
      sleep_enable();
      sleep_mode();
    
      sleep_disable();
      disableInterrupt(pir->getPin());
      start();
}

void SetupTask::tick(){
     if(millis()- startTime >= SLEEP_TIME){
        this->sleep();
     }
     if(btn->isPressed()){
      experiment->setActive(true);
      this->setActive(false);
     }
}
