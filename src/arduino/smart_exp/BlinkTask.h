#ifndef __BLINKTASK__
#define __BLINKTASK__

#include "TaskImpl.h"
#include "Led.h"

/* Task that manages led blinking*/
class BlinkTask: public TaskImpl {

  int pin;
  Light* led;
  enum { ON, OFF} state;

protected:
  void start();
  void finish();

public:

  BlinkTask(int pin);  
  void init(int period);  
  void tick();
};

#endif
