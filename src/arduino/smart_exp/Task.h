#ifndef __TASK__
#define __TASK__

/*
 Interface that models a Task that can be active or not and change state dynamically
 */

class Task {
  
public:
  virtual void init(int period)=0;

  virtual void tick() = 0;

  virtual bool updateAndCheckTime(int basePeriod)=0;

  virtual bool isActive()=0;

  virtual void setActive(bool active)=0;  
};

#endif
