#include "EndExperimentTask.h"
#include "ButtonImpl.h"
#include "MsgService.h"
#include "MsgFactory.h"

EndExperimentTask::EndExperimentTask( int btnPin){
    this->btnStop=new ButtonImpl(btnPin);
  }

void EndExperimentTask :: setControlledTask(Task* blinkTask, Task* setupTask, Task* expTask){
    this->blinkTask=blinkTask;
    this->setupTask=setupTask;
    this->experimentTask=expTask;
}

void EndExperimentTask::start(){
  startTime=millis();
  state=EXPERIMENT;
}

void EndExperimentTask::finish(){
  blinkTask->setActive(false);
}

void EndExperimentTask :: init(int period){
    TaskImpl::init(period);
}

  
void EndExperimentTask :: tick(){
  switch(state){
    case EXPERIMENT:
      if(millis()-startTime>=MAX_TIME || btnStop->isPressed()){      
        this->blinkTask->setActive(true);
        this->experimentTask->setActive(false);
        state=VIEWER_WAIT;
        Msg* msg=MessageFactory.createNotificationMessage("End experiment");
        MsgService.sendMsg(msg);
        delete msg;
      }
    break;
    case VIEWER_WAIT:
        if(MsgService.isMsgAvailable()){
          Msg* msg=MsgService.receiveMsg();
          if(msg->getContent()=="OK"){
            this->setActive(false);
            this->blinkTask->setActive(false);
            this->setupTask->setActive(true);
          }
          delete msg;
        }
    break;
  }
}
