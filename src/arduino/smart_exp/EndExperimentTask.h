#ifndef __ENDEXPERIMENTTASK__
#define __ENDEXPERIMENTTASK__

#include "TaskImpl.h"
#include "Button.h"

#define MAX_TIME 20000

/*
 Task that controls if the experiments ends and then return to initial state
*/
class EndExperimentTask: public TaskImpl {

private:
  Button* btnStop;
  Task *blinkTask, *setupTask, *experimentTask;
  unsigned long startTime;
  enum {EXPERIMENT,VIEWER_WAIT} state;

  void start();
  void finish();
  
public:
  EndExperimentTask( int btnPin);
  void init(int period);
  void tick();
  void setControlledTask(Task* blinkTask, Task* setupTask, Task* expTask);
};

#endif
