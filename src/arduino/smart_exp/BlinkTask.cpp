#include "BlinkTask.h"

BlinkTask::BlinkTask(int pin){
  this->pin = pin;    
}

void BlinkTask::start(){
  led->switchOn();
  state = ON;
}
void BlinkTask::finish(){
  led->switchOff();
}
  
void BlinkTask::init(int period){
  TaskImpl::init(period);
  led = new Led(pin);    
}
  
void BlinkTask::tick(){
  switch (state){
    case OFF:
      led->switchOn();
      state = ON; 
      break;
    case ON:
      led->switchOff();
      state = OFF;
      break;
  }
}
