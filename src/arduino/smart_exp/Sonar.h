#ifndef __SONAR__
#define __SONAR__

class Sonar {

public:
  virtual void initialize(float temperature) = 0;
  virtual float getDistance() = 0;
};

#endif
