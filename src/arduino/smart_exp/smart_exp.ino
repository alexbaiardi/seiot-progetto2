/*
 Realizzato da Baiardi Alex, Baldi Thomas e Natali Marco
 */

#include "Scheduler.h"
#include "SetupTask.h"
#include "BlinkTask.h"
#include "ExperimentTask.h"
#include "EndExperimentTask.h"
#include "ErrorTask.h"

#define L1PIN 13
#define L2PIN 12
#define PIRPIN 5
#define BTNSTARTPIN 4
#define BTNSTOPPIN 8
#define POTPIN A0
#define SERVOPIN 9
#define ECHOPIN 11
#define TRIGPIN 10
#define TEMPPIN A1
#define ERROR_TIME 2000


Scheduler sched;

void setup() {
  Serial.begin(9600);
  
  BlinkTask* blinkT = new BlinkTask(L2PIN);
  SetupTask* setupT = new SetupTask(PIRPIN,L1PIN,BTNSTARTPIN);
  ExperimentTask* expT= new ExperimentTask(L2PIN, ECHOPIN, TRIGPIN, POTPIN, SERVOPIN, TEMPPIN);
  EndExperimentTask* endExpT= new EndExperimentTask(BTNSTOPPIN);
  ErrorTask* errT= new ErrorTask();

  /*Assign to each task the tasks to be controlled*/
  setupT->setControlledTask(expT);
  expT->setControlledTask(setupT, endExpT, errT);
  endExpT->setControlledTask(blinkT, setupT, expT);
  errT->setControlledTask(blinkT,setupT);
  
  /*Task initialization*/
  setupT->init(40);
  blinkT->init(400);
  expT->init(40);
  endExpT->init(40);
  errT->init(ERROR_TIME);

  /*Scheduler initialization*/
  sched.init(40);
  sched.addTask(setupT);
  sched.addTask(blinkT);
  sched.addTask(expT);
  sched.addTask(endExpT);
  sched.addTask(errT);

  /*Set the first task to be executed*/
  setupT->setActive(true);
  blinkT->setActive(false);
  expT->setActive(false);
  endExpT->setActive(false);
  errT->setActive(false);
}

void loop() {
  sched.schedule();
}
