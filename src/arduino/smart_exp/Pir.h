#ifndef __PIR__
#define __PIR__

class Pir {

public:
  virtual int getPin()=0;
  virtual bool movement() = 0;
};

#endif
