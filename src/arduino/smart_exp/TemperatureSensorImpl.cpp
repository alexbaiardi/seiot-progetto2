#include "TemperatureSensorImpl.h"
#include "Arduino.h"

TemperatureSensorImpl::TemperatureSensorImpl(int pin){
    this->pin = pin;
}

float TemperatureSensorImpl::getTemperature(){
    int value = analogRead(pin);
    float valueInVolt = value*VCC/1023;
    float valueInCelsius = valueInVolt/0.01;
    return valueInCelsius;
}
