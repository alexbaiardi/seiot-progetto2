#ifndef __ERRORTASK__
#define __ERRORTASK__

#include "TaskImpl.h"

/*
  Task that control system error event 
*/
class ErrorTask: public TaskImpl {

private:
  Task *blinkTask, *setupTask;
  unsigned long startTime;
  
  void start();
  void finish();
  
public:
  ErrorTask();
  void init(int period);
  void tick();
  void setControlledTask(Task* blinkTask, Task* setupTask);
};

#endif
