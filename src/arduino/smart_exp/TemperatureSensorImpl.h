#ifndef __TEMPERATURESENSORIMPL__
#define __TEMPERATURESENSORIMPL__
#define VCC ((float)5)

#include "TemperatureSensor.h"

class TemperatureSensorImpl: public TemperatureSensor {

public:
  TemperatureSensorImpl(int pin);
  float getTemperature();

private:
  int pin;

};
#endif
