package controller;

import view.Viewer;

public class Controller extends Thread{
    
    private final Viewer view;
    private final String port;
    private final int baud;

    
    public Controller(Viewer view, String port, int baud) {
        this.view = view;
        this.port = port;
        this.baud = baud;
    }


    public void run() {
        try {
            final CommChannel channel = new SerialCommChannel(port, baud);
            while(true) {
                final String message=channel.receiveMsg();
                System.out.println(message);
                final String[] data=message.split(";");
                switch (data[0]) {
                case "state":
                    view.updateState(data[1]);
                    break;
                case "data":
                    view.updateCharts(Double.parseDouble(data[1]), Double.parseDouble(data[2]), Double.parseDouble(data[3]), Double.parseDouble(data[4]));
                    break;
                case "notification":
                    view.notifyEnd(data[1]);
                    channel.sendMsg("OK");
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
}
