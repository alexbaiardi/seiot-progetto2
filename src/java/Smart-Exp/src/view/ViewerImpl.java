package view;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.CountDownLatch;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;

/**
 * 
 *
 */
public class ViewerImpl implements Initializable, Viewer {
	
    @FXML
    private LineChart<String, Number> LCSpeed;

    @FXML
    private LineChart<String, Number> LCAcceleration;

    @FXML
    private LineChart<String, Number> LCPosition;

    @FXML
    private Label lblState;

    @FXML
    private Label lbState;

    /**
     * Method to initialize charts.
     */
    public void initialize(final URL location, final ResourceBundle resources) {
        addSeries(LCSpeed, "Speed");
        addSeries(LCAcceleration, "Acceleration");
        addSeries(LCPosition, "Position");

        LCSpeed.setCreateSymbols(false);
        LCAcceleration.setCreateSymbols(false);
        LCPosition.setCreateSymbols(false);

    }
    
    /**
     * Private method to add the series to the charts.
     * @param linecharts
     * @param series
     */
    private void addSeries(final LineChart<String, Number> linecharts, String serie) {
        final Series<String, Number> series = new Series<String, Number>();
        series.setName(serie);
        linecharts.getData().add(series);
    }
    
    @Override
    public void updateCharts(double time, double distance, double speed, double acceleration) {
        Platform.runLater(() -> {
            LCPosition.getData().get(0).getData().add(new Data<String, Number>(String.valueOf(time), distance));
            LCSpeed.getData().get(0).getData().add(new Data<String, Number>(String.valueOf(time), speed));
            LCAcceleration.getData().get(0).getData().add(new Data<String, Number>(String.valueOf(time), acceleration));
        });
    }
    
    @Override
    public void updateState(String state) {
        Platform.runLater(()->lbState.setText(state));     
    }
    
    @Override
    public void notifyEnd(String message) {
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        Platform.runLater(() -> {
            ButtonType button;
            do {
                final Alert alert = new Alert(AlertType.CONFIRMATION, message, ButtonType.OK);
                final Optional<ButtonType> result = alert.showAndWait();
                button = result.orElse(ButtonType.CANCEL);
            } while (button != ButtonType.OK);
            LCPosition.getData().get(0).getData().clear();
            LCSpeed.getData().get(0).getData().clear();
            LCAcceleration.getData().get(0).getData().clear();
            countDownLatch.countDown();
        });
        try {
            countDownLatch.await(); //wait javafx thread
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
