package view;

import java.io.IOException;
import java.util.Optional;

import controller.Controller;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.stage.Stage;

public class MainView extends Application {

    private static String port;
    private static int baud;
    
    /**
     * 
     */
    public void start(final Stage stage) {
        final FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource("scene/Chartsfx.fxml"));
        Parent root;
        try {
            root = loader.load();
            final Scene scene = new Scene(root, 1100, 870);
            stage.setTitle("Smart-Exp");
            stage.setScene(scene);
            stage.show();
            final Controller c=new Controller(loader.getController(), port, baud);
            c.start();
            stage.setOnCloseRequest(e -> {
                Platform.exit();
                System.exit(0);
            });
        } catch (IOException e) {
            final Alert alert = new Alert(AlertType.ERROR);
            alert.setHeaderText("LOADING ERROR");
            alert.show();
            e.printStackTrace();
        }
    }

    /**
     * @param args
     */
    public static void main(final String[] args) {
        port=args[0];
        baud=Integer.valueOf(args[1]);
        launch(args);
    }
}

