package view;

/**
 * 
 */
public interface Viewer {
    /**
     * Update charts adding data in parameter.
     * 
     * @param time
     * @param distance
     * @param speed
     * @param acceleration
     */
    void updateCharts(double time, double distance, double speed, double acceleration);
    
    /**
     * Shows new state
     * @param state
     *          new state.
     */
    void updateState(String state);
    
    /**
     * Notify end of experiment
     * @param message
     */
    void notifyEnd(String message);
    
}